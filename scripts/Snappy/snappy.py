import boto3
import pandas as pd
import xlsxwriter
import argparse

parser = argparse.ArgumentParser(description='Snapshot/AMI/Volume Linker')
parser.add_argument('--xlsx', action="store", dest="xlsx", required=True)
args = parser.parse_args()
xlsx = args.xlsx
ami_id = ""
ami_cd = ""
volume_id = ""
vol_state = ""
vol_instance = ""
region = "us-east-1" #default
client = boto3.client('ec2', region_name=region)
df = pd.read_excel(xlsx)

def if_associated_to_amivar(client, snapshot_id):
  img = client.describe_images(
      Filters=[
          {'Name': 'block-device-mapping.snapshot-id', 'Values': [snapshot_id]}
      ]
  )
  print(img)
  try:
      ami_id = img['Images'][0]['ImageId']
      ami_cd = img['Images'][0]['CreationDate']
      return ami_id, ami_cd
  except IndexError:
      return "None", "N/A"


def if_associated_to_volumevar(client, snapshot_id):
  snap = client.describe_snapshots(
      Filters=[
          {'Name': 'snapshot-id', 'Values': [snapshot_id]}
      ]
  )

  try:
      vol_id = snap['Snapshots'][0]['VolumeId']
      vol = client.describe_volumes(
          Filters=[
              {'Name': 'volume-id', 'Values': [vol_id]}
          ]
      )
      vol_state = vol['Volumes'][0]['State']
      return vol_id, vol_state
  except IndexError:
      return "None", "N/A"


def if_associated_to_instancevar(client, vol_id):
  try:
      vol = client.describe_volumes(
          Filters=[
              {'Name': 'volume-id', 'Values': [vol_id]}
          ]
      )
      vol_instance = vol['Volumes'][0]['Attachments'][0]['InstanceId']
      return vol_instance
  except IndexError:
      return "None"


vals_list = df['Snapshot Id'].tolist()
for i in ["Associated_Instance", "Volume_State", "Volume_ID", "AMI_Created", "AMI"]:
  df.insert(3, i, '')
dfdict = df.to_dict(orient='records')
print(df)


index = 0

for snap_id in vals_list:
    print("----------------------------")
    region = dfdict[index]['Region Name']
    print("Row " + str(index) + " :: Processing " + str(snap_id) + " in region " + str(region))
    client = boto3.client('ec2', region_name=region)
    ami, creationDate = if_associated_to_amivar(client, snap_id)
    print("AMI: " + str(ami))
    volume_id, volume_state = if_associated_to_volumevar(client, snap_id)
    instance_id = if_associated_to_instancevar(client, volume_id)
    dfdict[index]['AMI']=ami
    dfdict[index]['AMI_Created']=creationDate
    dfdict[index]['Volume_ID']=volume_id
    dfdict[index]['Volume_State']=volume_state
    dfdict[index]['Associated_Instance']=instance_id
    index += 1

df2 = pd.DataFrame(dfdict)
print(df2)
writer_obj = pd.ExcelWriter('LINKED-' + xlsx, engine='xlsxwriter')
df2.to_excel(writer_obj, sheet_name='Sheet')
writer_obj.save()

